import math

def sigmoid(x):
    #return x
    return 1 / (1 + math.exp(-x))
