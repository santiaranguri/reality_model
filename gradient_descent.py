import random
import time

class GradientDescent():
    def __init__(self, mind):
        self.mind = mind
        self.running_times = 100
        self.learning_rate = 0.3
        self.step = 0.0001

    def gradient(self, examples):
        self.examples = examples
        for i in range(self.running_times):
            print ('Running time:', i, self.get_cost())
            self.mind.agent.env.drawer.draw()
            time.sleep(.05)
            for neuron in self.mind.neurons:
                neuron.treshold += self.step
                a = self.get_cost()
                neuron.treshold -= self.step
                b = self.get_cost()
                neuron.treshold -= self.learning_rate * (a - b) / self.step

                for j in range(len(neuron.input_neurons)):
                    neuron.input_neurons[j][1] += self.step
                    a = self.get_cost()
                    neuron.input_neurons[j][1] -= self.step
                    b = self.get_cost()
                    neuron.input_neurons[j][1] -= self.learning_rate * (a - b) / self.step

    def get_cost(self):
        return sum([self.get_error(ex) for ex in self.examples]) / len(self.examples)

    def get_error(self, example):
        self.mind.predict(example)
        cases = list(zip(self.mind.get_neurons('sensor', 'goal'), example[3:]))

        return sum([(n.output - correct_output) ** 2 for n, correct_output in cases])
