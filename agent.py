import time
from mind import Mind

class Agent(object):
    def __init__(self, env):
        self.env = env
        self.mind = Mind(self)
        self.examples_amount = 100

    def explore(self):
        examples = [self.get_example(i) for i in range(self.examples_amount)]
        self.mind.gradient_descent.gradient(examples)
        #print('GD eff:', sum([self.explore_test() for i in range(5)]) / 5)

    def get_example(self, i):
        time.sleep(.01)
        if i % 1 == 0:
            self.env.randomize_vel()
        percept = self.env.percept()
        self.env.move()
        self.env.drawer.draw()
        new_percept = self.env.percept()[:-2] #[:-2] is to remove the velocity
        return percept + new_percept

    def explore_test(self):
        self.env.randomize_agent_pos()
        example = self.get_example(0)
        self.mind.predict(example)
        cases = list(zip(self.mind.get_neurons('sensor', 'goal'), example[3:]))
        print (example)
        for n, correct in cases:
            print (n.output, correct, n.output - correct)
        return sum([(n.output - correct) ** 2 for n, correct in cases])

    def live(self):
        example = self.env.percept()
        self.mind.predict(example)

        while True:
            self.mind.init_water()

            for i in range(8):
                print ('step', i)
                self.env.drawer.draw()
                time.sleep(.2)
                charged_neurons = [n for n in self.mind.charged_neurons]
                for neuron in charged_neurons:
                    neuron.distribute_water()

            for i, actuator in enumerate(self.mind.get_neurons('actuator')):
                if actuator.water[0] > actuator.water[1]:
                    self.env.agent_vel[i] = 1
                else:
                    self.env.agent_vel[i] = 0

            self.env.move()
            example = self.env.percept()
            self.mind.fill_net(example)
            #print ((self.env.agent_pos[0] - self.env.agent_pos[1]) / 2)
            self.env.drawer.draw()
            time.sleep(.1)
