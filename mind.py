import random
import math
import utils
from gradient_descent import GradientDescent
from operator import add

class Mind:
    def __init__(self, agent):
        self.agent = agent
        self.gradient_descent = GradientDescent(self)
        self.charged_neurons = [] #Neurons with water
        #self.neuron_types = ['sensor'] + ['sensor'] + ['goal'] + ['actuator'] + ['actuator']
        self.neuron_types = ['goal'] + ['actuator'] + ['actuator']
        self.neurons = [Neuron(self, type) for type in self.neuron_types]
        for neuron in self.get_neurons('sensor', 'goal'):
            neuron.connect_randomly()

    def get_neurons(self, type, other_type=None):
        return filter(lambda n: n.type in [type, other_type], self.neurons)

    def fill_net(self, example):
        for i, neuron in enumerate(self.neurons):
            neuron.output = example[i]

    def predict(self, example):
        self.fill_net(example)

        for neuron in self.neurons:
            weighted_sum = sum([n.output * w for n, w in neuron.input_neurons])
            neuron.next_output = utils.sigmoid(weighted_sum)# - neuron.treshold)

        for neuron in self.neurons:
            neuron.output = neuron.next_output

    def init_water(self):
        for neuron in self.neurons:
            neuron.remove_water()
        self.charged_neurons = []
        goal_neuron = list(self.get_neurons('goal'))[0]
        goal_neuron.receive_water([0, 1])

    def water_test(self):
        for i, neuron in enumerate(self.neurons):
            print('Neuron', i, 'water', neuron.water)

class Neuron:
    def __init__(self, mind, type):
        self.mind = mind
        self.type = type

        self.input_neurons = []
        self.output = 0
        self.treshold = 0#random.uniform(0, 1)
        self.water = [0, 0]

    def connect_randomly(self):
        for neuron in self.mind.neurons:
            weight = 0#random.uniform(-1, 1)#0
            '''
            if self.type == 'goal' and neuron.type == 'sensor':
                weight = 1.3#random.uniform(-1, 1)
            elif self.type == 'sensor' and neuron.type == 'sensor':
                weight = 1.2#random.uniform(-1, 1)
            elif self.type == 'sensor' and neuron.type == 'actuator':
                weight = 1/5#random.uniform(-1, 1)
            elif self.type == 'goal' and neuron.type == 'actuator':
                weight = 1/4#random.uniform(-1, 1)
            '''
            self.input_neurons.append([neuron, weight])

    def remove_water(self):
        self.water = [0, 0]
        if self in self.mind.charged_neurons:
            self.mind.charged_neurons.remove(self)

    def receive_water(self, added_water):
        self.water = list(map(add, self.water, added_water))
        if self.type != 'actuator':
            self.mind.charged_neurons.append(self)

    def transfer_water(self, next_neuron):
        next_neuron.receive_water(self.water)
        self.remove_water()

    def potential(self, neuron, weight, type):
        return abs(weight)
        '''
        print ('connection', self.type, neuron.type, weight, type)
        if (weight > 0 and type == 'pos') or (weight <= 0 and type == 'neg'):
            return weight#(1 - neuron.output) * weight
        elif (weight <= 0 and type == 'pos') or (weight > 0 and type == 'neg'):
            return -1 * weight#(0 - neuron.output) * weight
        '''

    def distribute_water(self):
        neuron_water = self.water
        self.remove_water()

        if neuron_water[0] != 0:
            total_potential = sum([self.potential(n, w, 'pos') for n, w in self.input_neurons])
            for neuron, weight in self.input_neurons:
                relative_water = self.potential(neuron, weight, 'pos') / total_potential
                water = neuron_water[0] * relative_water
                if weight > 0:
                    neuron.receive_water([water, 0])
                elif weight <= 0:
                    neuron.receive_water([0, water])

        if neuron_water[1] != 0:
            total_potential = sum([self.potential(n, w, 'neg') for n, w in self.input_neurons])
            for neuron, weight in self.input_neurons:
                relative_water = self.potential(neuron, weight, 'neg') / total_potential
                water = neuron_water[1] * relative_water
                if weight > 0:
                    neuron.receive_water([0, water])
                elif weight <= 0:
                    neuron.receive_water([water, 0])
