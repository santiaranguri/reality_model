import random
from agent import Agent
from drawer import Drawer

class Env(object):
    def __init__(self):
        self.agent = Agent(self)
        self.drawer = Drawer(self)
        self.food_pos = (0, 0)
        self.agent_pos = [random.uniform(0, 1) for _ in range(2)]
        self.agent_vel = [random.uniform(0, 1) for _ in range(2)]

    def main(self):
        self.agent.explore()
        self.agent.live()

    def percept(self):
        #distance_to_food = [(self.food_pos[i] - self.agent_pos[i] + 1) / 2 for i in range(2)]
        #goal = (self.agent_pos[0] - self.agent_pos[1]) / 2
        goal = self.agent_pos[0] + self.agent_pos[1]#) / 2
        #return self.agent_pos + [goal] + self.agent_vel #distance_to_food
        return [goal] + self.agent_vel #distance_to_food

    def randomize_agent_pos(self):
        self.agent_pos = [random.uniform(0, 1) for _ in range(2)]
        self.agent.mind.fill_net(self.percept)

    def randomize_vel(self):
        self.agent_vel = [random.uniform(0, 1) for _ in range(2)]

    def move(self):
        for i in range(2):
            print ('agent_pos', i, self.agent_pos[i])
            self.agent_pos[i] += (self.agent_vel[i] - 0.5) * 2 / 7
            if not (0 < self.agent_pos[i] < 1):
                self.agent_pos[i] = int(self.agent_pos[i])

Env().main()
