class MindDrawer:
    def __init__(self, core_drawer, width, height):
        self.core_drawer = core_drawer
        self.width = width
        self.height = height

    def draw_mind(self):
        layers = self.core_drawer.mind.layers
        screen = self.core_drawer.screen

        for index_layer, layer in enumerate(layers):

            width = (1 + (index_layer / len(layers))) * self.width
            for index_neuron, neuron in enumerate(layer.neurons):
                height = index_neuron / len(layer.neurons) * self.height
                neuron.map_pos = (int(width), int(height))
                pygame.draw.circle(screen, WHITE, neuron.map_pos, 10)
                prev_layer = layers[layer.pos - 1]

                for connection in neuron.connections:
                    weight = connection[1]
                    prev_neuron = prev_layer.neurons[connection[0]]
                    color = GREEN if weight > 0 else RED
                    size = int(5 * abs(weight))
                    size = size if size != 0 else 1
                    pygame.draw.line(screen, color, prev_neuron.map_pos, neuron.map_pos, size)
