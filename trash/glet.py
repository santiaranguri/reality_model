import itertools
import pygame
import time

BLUE = (0, 0, 255)
WHITE = (255, 255, 255)
(width, height) = (1366, 768)

# Set up screen
screen = pygame.display.set_mode((width, height))
pygame.draw.line(screen, WHITE, (width / 2, 0), (width / 2, height), 3)

# Draw axis
pygame.draw.line(screen, WHITE, (0, height / 2), (width / 2, height / 2), 1)
pygame.draw.line(screen, WHITE, (width / 4, 0), (width / 4, height), 1)

for i in itertools.count():
	time.sleep(1)
	pos = (i, 10)
	pygame.draw.circle(screen, BLUE, pos, 5)
	pygame.display.flip()
